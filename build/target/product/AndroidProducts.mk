#
# Copyright (C) 2018-2019 The LineageOS Project
#           (C) 2020 The AYSecurity Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/aysecurity_arm.mk \
    $(LOCAL_DIR)/aysecurity_arm64.mk \
    $(LOCAL_DIR)/aysecurity_x86.mk \
    $(LOCAL_DIR)/aysecurity_x86_64.mk

COMMON_LUNCH_CHOICES := \
    aysecurity_arm-userdebug \
    aysecurity_arm64-userdebug \
    aysecurity_x86-userdebug \
    aysecurity_x86_64-userdebug
